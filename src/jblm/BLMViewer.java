/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jblm;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.ISpectrumListener;
import fr.esrf.tangoatk.core.NumberSpectrumEvent;
import fr.esrf.tangoatk.core.attribute.NumberSpectrum;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.LoadSaveFileAdapter;
import fr.esrf.tangoatk.widget.util.SettingsManagerProxy;
import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLAxis;
import fr.esrf.tangoatk.widget.util.chart.JLChart;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author pons
 */
public class BLMViewer extends JPanel implements ISpectrumListener,
        IJLChartListener,ActionListener {

  static Color[] cellColors = new Color[] {
      new Color(80,255,80),
      new Color(80,80,255),
      new Color(255,160,255),
      new Color(200,200,255),      
  };

  String settingManagerName;
  final String refName;
  JLDataView dv;
  JLDataView dvRef;
  double[] refData = null;
  JLChart    chart;
  NumberSpectrum model = null;
  NumberSpectrum refModel = null;
  SettingsManagerProxy smRef = null;
  JPanel downPanel;
  JCheckBox logScaleCheck;
  JComboBox refCombo;
  JButton loadRefBtn;
  JButton saveRefBtn;
  JTextField refText;
  int displayMode = 0;
  String name;
  
  public BLMViewer(String name,String _refName,String refSettingManagerName) {
    
    this.name = name;
    refName = _refName;
    settingManagerName = refSettingManagerName;
    
    dv = new JLDataView();
    dv.setName(name);
    dv.setBarWidth(5);
    dv.setColor(Color.BLACK);
    dv.setViewType(JLDataView.TYPE_BAR);
    dv.setFillStyle(JLDataView.FILL_STYLE_SOLID);
    

    setLayout(new BorderLayout());    
    
    chart = new JLChart();
    
    chart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
    chart.getXAxis().setAutoScale(false);
    chart.getXAxis().setMinimum(-1);
    chart.getXAxis().setMaximum(128);
    
    String[] cellNames = new String[32];
    double[] cellPos = new double[32];
    for(int i=0;i<cellNames.length;i++) {
      cellNames[i] = new String("C" + Integer.toString(i+1));
      cellPos[i] = (double)i*4;
    }
    chart.getXAxis().setLabels(cellNames, cellPos);
    chart.getXAxis().setGridVisible(true);
    chart.getY1Axis().setAutoScale(true);
    chart.getY1Axis().addDataView(dv);
    chart.getY1Axis().setGridVisible(true);
    
    setPreferredSize(new Dimension(1100,300));
                                    
    
    chart.setJLChartListener(this);


    if (refName!=null) {

      downPanel = new JPanel();
      downPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
      
      // Setting manager API
      smRef = new SettingsManagerProxy(refSettingManagerName);
      smRef.setErrorHistoryWindow(MainPanel.errWin);
      smRef.setLoadButtonText("Load ref");
      smRef.setSaveButtonText("Save ref");
      smRef.setLoadSaveListener(new LoadSaveFileAdapter() {
        
        public void beforeSave(SettingsManagerProxy src, File f) {

          // Update reference attribute
          try {

            DeviceProxy ds = new DeviceProxy(MainPanel.BLMAllName);
            DeviceAttribute rd = ds.read_attribute(model.getNameSansDevice());
            DeviceAttribute wd = new DeviceAttribute(refName);
            refData = rd.extractDoubleArray();
            updateRef();
            wd.insert(refData);
            ds.write_attribute(wd);

          } catch (DevFailed ex) {
            ErrorPane.showErrorMessage(null, settingManagerName, ex);
          }

        }

        public void afterLoad(SettingsManagerProxy src, File f) {

          // Update
          try {
            loadRef();
            updateRef();
          } catch (DevFailed ex) {
            ErrorPane.showErrorMessage(null, settingManagerName, ex);
          }

        }
      });

      
      dvRef = new JLDataView();
      dvRef.setName(name + "Ref");
      dvRef.setBarWidth(6);
      dvRef.setColor(new Color(255, 0, 0));
      dvRef.setClickable(false);

      loadRefBtn = new JButton("Load Ref");
      loadRefBtn.addActionListener(this);
      downPanel.add(loadRefBtn);

      saveRefBtn = new JButton("Save Ref");
      saveRefBtn.addActionListener(this);
      downPanel.add(saveRefBtn);

      refText = new JTextField();
      refText.setEditable(false);
      refText.setText("None");
      refText.setPreferredSize(new Dimension(150,25));
      downPanel.add(refText);

      refCombo = new JComboBox();
      refCombo.addItem("Ignore");
      refCombo.addItem("Superimpose");
      refCombo.addItem("Substract");
      refCombo.addActionListener(this);
      downPanel.add(refCombo);

      logScaleCheck = new JCheckBox("Log scale");
      logScaleCheck.addActionListener(this);
      logScaleCheck.setSelected(false);
      downPanel.add(logScaleCheck);

      add(downPanel,BorderLayout.SOUTH);
     
      // Load current ref
      try {
        loadRef();
        updateRef();
      } catch (DevFailed ex) {
      }
      refText.setText(smRef.getLastAppliedFile());

    }
    
    
    
    add(chart,BorderLayout.CENTER);
    
  }
  
  public void clearModel() {
    if(model!=null) {
      model.removeSpectrumListener(this);
      model = null;
    }
  }
  public void setModel(NumberSpectrum model) {
    clearModel();
    this.model = model;
    if( model!=null)
      model.addSpectrumListener(this);
  }
  
  boolean isRefValid() {
    return refData != null && refData.length==128;
  }
  
  @Override
  public void spectrumChange(NumberSpectrumEvent nse) {
    
    double[] vals = nse.getValue();
    dv.reset();
    for(int i=0;i<vals.length;i++)
      if(displayMode==2 && isRefValid())
        dv.add((double)i,vals[i]-refData[i],false);
      else
        dv.add((double)i,vals[i],false);
    dv.commitChange();
    
    for(int i=0;i<128;i++)
      dv.setBarFillColorAt(i, cellColors[i%4]);
    
    repaint();
    
  }

  @Override
  public void stateChange(AttributeStateEvent ase) {
  }

  @Override
  public void errorChange(ErrorEvent ee) {
    dv.reset();
    repaint();
  }

  @Override
  public String[] clickOnChart(JLChartEvent jlce) {
    
    int idx = jlce.getDataViewIndex();
    int cell = idx/4 + 1;
    int blmIdx = (idx%4) + 1;
    
    if(name.toLowerCase().indexOf("losses")!=-1) {
      
      String[] ret = new String[3];
      ret[0] = "BLM C" + cell + "-" + blmIdx + "(#" + idx + ")";
      ret[1] = Double.toString(jlce.getYValue());
      if(blmIdx==1) {
        ret[2] = "Losses after straight section";
      } else {
        ret[2] = "Losses in the arc";
      }
      return ret;
      
    } else {
      
      String[] ret = new String[2];
      ret[0] = "BLM C" + cell + "-" + blmIdx + "(#" + idx + ")";
      ret[1] = Double.toString(jlce.getYValue());
      return ret;
      
    }
    
    
    
  }
  
  private void loadRef() throws DevFailed {
    
    DeviceProxy ds = new DeviceProxy(MainPanel.BLMAllName);
    DeviceAttribute rd = ds.read_attribute(refName);
    double[] data = rd.extractDoubleArray();
    System.out.println("Ref:" + rd.getNbRead() + " pts");
    refData = new double[rd.getNbRead()];
    for (int i = 0; i < refData.length; i++) {
      refData[i] = data[i];
    }
    
  }
  
  private void updateRef() {
    dvRef.reset();
    if(isRefValid())
      for(int i=0;i<refData.length;i++) {
        double pos = (double)i;
        dvRef.add(pos - 0.5,refData[i]);
        dvRef.add(pos + 0.5,refData[i]);
      }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Object src = e.getSource();
    if(src==logScaleCheck) {
      
      if(logScaleCheck.isSelected())
        chart.getY1Axis().setScale(JLAxis.LOG_SCALE);
      else
        chart.getY1Axis().setScale(JLAxis.LINEAR_SCALE);
      
    } else if ( src==saveRefBtn ) {
      
      smRef.saveSettingsFile();
      refText.setText(smRef.getLastAppliedFile());
      repaint();
           
    } else if (src==loadRefBtn ) {

      // Load ref
      smRef.loadSettingsFile();
      refText.setText(smRef.getLastAppliedFile());
      
    } else if (src==refCombo) {
      
      displayMode = refCombo.getSelectedIndex();
      dvRef.removeFromAxis();
      switch(displayMode) {
        case 0: // None
          break;
        case 1:
          chart.getY1Axis().addDataViewAt(0,dvRef);
          break;
        case 2:
          break;          
      }
      
    }
    
  }
  
}
