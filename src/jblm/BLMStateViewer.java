/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jblm;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.DevStateSpectrumEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IDevStateSpectrumListener;
import fr.esrf.tangoatk.core.attribute.DevStateSpectrum;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

/**
 *
 * @author pons
 */
public class BLMStateViewer extends JPanel implements IDevStateSpectrumListener {

  final static Font btnFont = new Font("Dialog",Font.PLAIN,12);
  final static Insets NULLINSETS = new Insets(0,0,0,0);
  
  JButton[] allStates;
  JCheckBox[] allDisables;
  DevStateSpectrum model = null;
  
  
  public BLMStateViewer() {

    setLayout(new GridLayout(1,32));
    setBorder(BorderFactory.createEtchedBorder());

    allStates = new JButton[32];
    allDisables = new JCheckBox[32];
    
    for(int i=0;i<32;i++) {
      
      final short blmIdx = (short)i;

      JPanel innerPanel = new JPanel();
      innerPanel.setLayout(new GridBagLayout());
      GridBagConstraints gbc = new GridBagConstraints();
      gbc.gridx = 0;
      gbc.gridy = 0;
      gbc.weightx = 1.0;
      gbc.fill = GridBagConstraints.BOTH;
      
      
      allStates[i] = new JButton("C"+Integer.toString(i+1));
      allStates[i].setFont(btnFont);
      allStates[i].setMargin(NULLINSETS);
      final String blmName = "srdiag/blm/C"+String.format("%02d",i+1);
      allStates[i].addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          launchPanel(blmName);
        }
      });
      innerPanel.add(allStates[i],gbc);
      
      allDisables[i] = new JCheckBox("");
      allDisables[i].setSelected(true);
      allDisables[i].addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          
          try {
            DeviceData dd = new DeviceData();
            dd.insert(blmIdx);
            if(allDisables[blmIdx].isSelected())
              model.getDevice().command_inout("EnableDevice", dd);
            else
              model.getDevice().command_inout("DisableDevice", dd);
          } catch(DevFailed ex) {
            ErrorPane.showErrorMessage(null, MainPanel.BLMAllName, ex);
          }
          
        }
      });
      gbc.fill = GridBagConstraints.NONE;
      gbc.gridy = 1;
      innerPanel.add(allDisables[i],gbc);
      
      add(innerPanel);      
      
    }
    setUnknown();
    
  }
  
  public void setToggleVisible(boolean visible) {
    
    for(int i=0;i<allDisables.length;i++)
      allDisables[i].setVisible(visible);
    
  }
  
  public void clearModel() {
    if(model!=null) {
      model.removeDevStateSpectrumListener(this);
      model = null;
    }
  }
  public void setModel(DevStateSpectrum model) {
    clearModel();
    this.model = model;
    if( model!=null)
      model.addDevStateSpectrumListener(this);
  }

  private void setUnknown() {
    for(int i=0;i<allStates.length;i++)
      allStates[i].setBackground(ATKConstant.getColor4State("UNKNOWN"));    
  }

  public void devStateSpectrumChange(DevStateSpectrumEvent dsse) {
    String[] values = dsse.getValue();
    for(int i=0;i<allStates.length;i++) {
      allStates[i].setBackground(ATKConstant.getColor4State(values[i]));    
      allDisables[i].setSelected(!values[i].equalsIgnoreCase("DISABLE"));
    }
  }

  public void stateChange(AttributeStateEvent ase) {
  }

  public void errorChange(ErrorEvent ee) {
    setUnknown();
  }
  
  public void launchPanel(String devName) {
      new atkpanel.MainPanel(devName, false, true, true);
  }
  
  
  
}
