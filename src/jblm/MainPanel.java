/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jblm;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.BooleanScalarEvent;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IBooleanScalarListener;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.IEntityFilter;
import fr.esrf.tangoatk.core.attribute.BooleanScalar;
import fr.esrf.tangoatk.core.attribute.DevStateSpectrum;
import fr.esrf.tangoatk.core.attribute.EnumScalar;
import fr.esrf.tangoatk.core.attribute.NumberScalar;
import fr.esrf.tangoatk.core.attribute.NumberSpectrum;
import fr.esrf.tangoatk.core.attribute.StringScalar;
import fr.esrf.tangoatk.core.attribute.StringSpectrum;
import fr.esrf.tangoatk.widget.attribute.NumberSpectrumViewer;
import fr.esrf.tangoatk.widget.attribute.ScalarListViewer;
import fr.esrf.tangoatk.widget.attribute.SimpleStringSpectrumViewer;
import fr.esrf.tangoatk.widget.attribute.Trend;
import fr.esrf.tangoatk.widget.util.ATKDiagnostic;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.SettingsManagerProxy;
import fr.esrf.tangoatk.widget.util.Splash;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 *
 * @author pons
 */
public class MainPanel extends javax.swing.JFrame implements IBooleanScalarListener {

  static public ErrorHistory errWin;
  final static String APP_RELEASE = "1.18";
  final static String BLMAllName = "srdiag/blm/all";
  final static String offsetsSettingManagerName = "sys/settings/blm-offset";
  final static String gainsSettingManagerName = "sys/settings/blm-gain";

  private boolean runningFromShell=true;
  private Splash splash;

  private AttributeList attList;
  private AttributeList setAttList;
  
  private BLMViewer lossesViewer;
  private BLMViewer injLossesViewer;
  private BLMViewer vgcViewer;
  private BLMViewer attViewer;
  private BLMViewer maxAdcViewer;
  private BLMViewer offsetViewer;
  private BLMStateViewer stateViewer;
  private Trend totalLossViewer;
  private NumberSpectrumViewer trLossViewer;
  private SimpleStringSpectrumViewer errorViewer;  
  private JScrollPane settingsPane;
  private ScalarListViewer settings;
  private SettingsManagerProxy smOffsets;
  private SettingsManagerProxy smGains;

  private boolean lastInjection = false;

  public MainPanel() {
    this(true);
  }
  
  /**
   * Creates new form MainPanel
   */
  public MainPanel(boolean runningFromShell) {
    
    // Handle windowClosing (Close from the system menu)
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        exitForm();
      }
    });
    
    initComponents();
    // Global variables
    
    this.runningFromShell = runningFromShell;
    
    errWin = new ErrorHistory();
    
    attList = new AttributeList();
    attList.addErrorListener(errWin);
    setAttList = new AttributeList();
    setAttList.addErrorListener(errWin);
    setAttList.addSetErrorListener(ErrorPopup.getInstance());
    setAttList.setFilter(new IEntityFilter() {
      public boolean keep(IEntity ie) {
        return (ie instanceof NumberScalar) || 
               (ie instanceof BooleanScalar) ||
               (ie instanceof EnumScalar);
      }
    });

    // Splash window
    
    splash = new Splash();
    splash.setTitle("BLM " + APP_RELEASE);
    splash.setCopyright("(c) ESRF 2017");
    splash.setVisible(true);
    splash.setMaxProgress(15);
    splash.progress(0);
    
    lossesViewer = new BLMViewer("Losses","LossesRef","sys/settings/blm-ref");
    tabPane.add("Losses",lossesViewer);
    injLossesViewer = new BLMViewer("Injection Losses","InjLossesRef","sys/settings/blm-injref");
    tabPane.add("Injection Losses",injLossesViewer);
    
    totalLossViewer = new Trend();
    totalLossViewer.getChart().getXAxis().setGridVisible(true);
    totalLossViewer.getChart().getY1Axis().setGridVisible(true);
    totalLossViewer.getChart().setBackground(Color.WHITE);
    totalLossViewer.getChart().setDisplayDuration(900000);
    totalLossViewer.addAttribute(BLMAllName + "/TotalLoss");
    totalLossViewer.addAttribute(BLMAllName + "/TotalInjectionLoss");
    totalLossViewer.addToAxis(BLMAllName + "/TotalLoss",Trend.SEL_Y1,false);
    totalLossViewer.addToAxis(BLMAllName + "/TotalInjectionLoss",Trend.SEL_Y2,false);
    totalLossViewer.getDataViewForAttribute(BLMAllName + "/TotalInjectionLoss").setColor(Color.BLUE);
    totalLossViewer.getDataViewForAttribute(BLMAllName + "/TotalInjectionLoss").setLineWidth(2);
    totalLossViewer.getDataViewForAttribute(BLMAllName + "/TotalLoss").setLineWidth(2);    
    totalLossViewer.setButtonBarVisible(false);
    totalLossViewer.setSelectionTreeVisible(false);
    totalLossViewer.setPreferredSize(new Dimension(600,250));
    totalLossViewer.getModel().setRefreshInterval(250);
    totalLossViewer.getModel().startRefresher();
    trendPanel.add(totalLossViewer,BorderLayout.CENTER);    
    
    vgcViewer = new BLMViewer("VGC",null,null);
    tabPane.add("VGC",vgcViewer);
    attViewer = new BLMViewer("Attenuation",null,null);
    tabPane.add("Attenuation",attViewer);
    maxAdcViewer = new BLMViewer("Max ADC",null,null);
    tabPane.add("Max ADC",maxAdcViewer);
    offsetViewer = new BLMViewer("Offsets",null,null);
    tabPane.add("Offsets",offsetViewer);
    
    trLossViewer = new NumberSpectrumViewer();
    trLossViewer.setBackground(Color.WHITE);
    trLossViewer.setXAxisAffineTransform(0.0 , 2.0*2.816);
    trLossViewer.setLabelVisible(false);
    trLossViewer.getXAxis().setName("Time (us)");
    trLossViewer.getXAxis().setGridVisible(true);
    trLossViewer.getY1Axis().setGridVisible(true);
    tabPane.add("Time Resolved Inj Loss",trLossViewer);
    
    
    settings = new ScalarListViewer();
    settingsPane = new JScrollPane(settings);
    settingsPane.setPreferredSize(new Dimension(800,300));
    tabPane.add("Settings",settingsPane);    
    
    errorViewer = new SimpleStringSpectrumViewer();
    tabPane.add("Errors",errorViewer);
     
                
    stateViewer = new BLMStateViewer();
    downPanel.add(stateViewer,BorderLayout.NORTH);
    
    
    
    int nbDev=0;
    try {
      
      NumberSpectrum losses = (NumberSpectrum)attList.add(BLMAllName + "/Losses");
      lossesViewer.setModel(losses);      
      splash.progress(nbDev++);

      NumberScalar totalLoss = (NumberScalar)attList.add(BLMAllName + "/TotalLoss");
      totalLossScalarViewer.setModel(totalLoss);
      totalLossScalarViewer.setBackgroundColor(Color.WHITE);
      splash.progress(nbDev++);

      NumberSpectrum injlosses = (NumberSpectrum)attList.add(BLMAllName + "/InjLosses");
      injLossesViewer.setModel(injlosses);
      splash.progress(nbDev++);

      NumberScalar totalInjLoss = (NumberScalar)attList.add(BLMAllName + "/TotalInjectionLoss");
      totalInjLossScalarViewer.setModel(totalInjLoss);
      totalInjLossScalarViewer.setBackgroundColor(Color.WHITE);
      splash.progress(nbDev++);

      NumberSpectrum vgc = (NumberSpectrum)attList.add(BLMAllName + "/VGC");
      vgcViewer.setModel(vgc);
      splash.progress(nbDev++);

      NumberSpectrum att = (NumberSpectrum)attList.add(BLMAllName + "/Attenuations");
      attViewer.setModel(att);
      splash.progress(nbDev++);

      NumberSpectrum adc = (NumberSpectrum)attList.add(BLMAllName + "/MaxADC");
      maxAdcViewer.setModel(adc);
      splash.progress(nbDev++);
      
      NumberSpectrum offsets = (NumberSpectrum)attList.add(BLMAllName + "/Offsets");
      offsetViewer.setModel(offsets);
      splash.progress(nbDev++);
      
      NumberSpectrum trLoss = (NumberSpectrum)attList.add(BLMAllName + "/TimeResolvedInjLoss");
      trLossViewer.setModel(trLoss);
      trLoss.refresh();
      
      DevStateSpectrum states = (DevStateSpectrum)attList.add(BLMAllName + "/States");
      stateViewer.setModel(states);
      splash.progress(nbDev++);

      StringScalar blmStatus = (StringScalar)attList.add(BLMAllName + "/Status");
      blmStatusViewer.setModel(blmStatus);
      splash.progress(nbDev++);
      
      StringScalar gainFile = (StringScalar)attList.add(gainsSettingManagerName+"/LastAppliedFile");
      gainFileViewer.setBackgroundColor(getBackground());
      gainFileViewer.setModel(gainFile);
      splash.progress(nbDev++);
      
      StringScalar offsetFile = (StringScalar)attList.add(offsetsSettingManagerName+"/LastAppliedFile");
      offsetFileViewer.setBackgroundColor(getBackground());
      offsetFileViewer.setModel(offsetFile);
      splash.progress(nbDev++);
      
      EnumScalar injMode = (EnumScalar)attList.add(BLMAllName+"/InjectionMode");
      injectionComboEditor.setEnumModel(injMode);
      splash.progress(nbDev++);

      BooleanScalar calibMode = (BooleanScalar)attList.add(BLMAllName+"/AutoCalibration");
      autoCalibComboEditor.setAttModel(calibMode);
      splash.progress(nbDev++);

      BooleanScalar zeroGainMode = (BooleanScalar)attList.add(BLMAllName+"/ZeroGainNoCurrent");
      zeroGainComboEditor.setAttModel(zeroGainMode);
      splash.progress(nbDev++);
            
      setAttList.add(BLMAllName+"/*");
      settings.setModel(setAttList);
      splash.progress(nbDev++);
      
      StringSpectrum err = (StringSpectrum)attList.add(BLMAllName + "/Errors");
      errorViewer.setModel(err);              
      splash.progress(nbDev++);
      
      BooleanScalar injInProgress = (BooleanScalar)attList.add(BLMAllName+"/InjectionInProgress");
      injInProgress.addBooleanScalarListener(this);
            
    } catch( ConnectionException e ) {      
    }
    
    // Setting manager API

    smOffsets = new SettingsManagerProxy(offsetsSettingManagerName);
    smOffsets.setErrorHistoryWindow(errWin);
    smOffsets.setLoadButtonText("Load Offsets");
    smOffsets.setSaveButtonText("Save Offsets");

    smGains = new SettingsManagerProxy(gainsSettingManagerName);
    smGains.setErrorHistoryWindow(errWin);
    smGains.setLoadButtonText("Load Gains");
    smGains.setSaveButtonText("Save Gains");
    
    stateViewer.setToggleVisible(false);
    btnPanel.setVisible(false);
    settingPanel.setVisible(false);
    
    splash.progress(100);
    try{ Thread.sleep(100); } catch( InterruptedException e) {}
    attList.startRefresher();
    setAttList.startRefresher();
    
    splash.setVisible(false);    
    setTitle("BLM " + APP_RELEASE);
    ATKGraphicsUtils.centerFrameOnScreen(this);
    
  }
    
  /**
   * Exit the application
   */
  public void exitForm() {

    if (runningFromShell) {

      System.exit(0);

    } else {

      clearModel();
      setVisible(false);
      dispose();

    }

  }
  
  public void clearModel() {
    
    lossesViewer.clearModel();
    vgcViewer.clearModel();
    maxAdcViewer.clearModel();
    offsetViewer.clearModel();
    stateViewer.clearModel();
    errorViewer.setModel(null);
    settings.setModel(null);
    
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {
    java.awt.GridBagConstraints gridBagConstraints;

    centerPanel = new javax.swing.JPanel();
    jSplitPane1 = new javax.swing.JSplitPane();
    tabPane = new javax.swing.JTabbedPane();
    totalLossPanel = new javax.swing.JPanel();
    trendPanel = new javax.swing.JPanel();
    valuePanel = new javax.swing.JPanel();
    jSmoothLabel1 = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    totalLossScalarViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    jSmoothLabel2 = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    totalInjLossScalarViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    downPanel = new javax.swing.JPanel();
    statusPanel = new javax.swing.JPanel();
    blmStatusViewer = new fr.esrf.tangoatk.widget.attribute.StatusViewer();
    settingPanel = new javax.swing.JPanel();
    gainFileViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    offsetFileViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    gainButton = new javax.swing.JButton();
    offsetButton = new javax.swing.JButton();
    btnPanel = new javax.swing.JPanel();
    dummyPanel = new javax.swing.JPanel();
    zeroButton = new javax.swing.JButton();
    resetButton = new javax.swing.JButton();
    EnableAllButton = new javax.swing.JButton();
    DisableAllButton = new javax.swing.JButton();
    resetTriggerButton = new javax.swing.JButton();
    StartButton = new javax.swing.JButton();
    StopButton = new javax.swing.JButton();
    btnSettingPanel = new javax.swing.JPanel();
    jLabel5 = new javax.swing.JLabel();
    zeroGainComboEditor = new fr.esrf.tangoatk.widget.attribute.BooleanScalarComboEditor();
    jLabel4 = new javax.swing.JLabel();
    autoCalibComboEditor = new fr.esrf.tangoatk.widget.attribute.BooleanScalarComboEditor();
    jLabel3 = new javax.swing.JLabel();
    injectionComboEditor = new fr.esrf.tangoatk.widget.attribute.EnumScalarComboEditor();
    jMenuBar1 = new javax.swing.JMenuBar();
    fileMenu = new javax.swing.JMenu();
    loadOffsetMenuItem = new javax.swing.JMenuItem();
    saveOffsetMenuItem = new javax.swing.JMenuItem();
    loadGainMenuItem = new javax.swing.JMenuItem();
    saveGainMenuItem = new javax.swing.JMenuItem();
    exitMenuItem = new javax.swing.JMenuItem();
    viewMenu = new javax.swing.JMenu();
    expertMenuItem = new javax.swing.JCheckBoxMenuItem();
    diagMenuItem = new javax.swing.JMenuItem();
    errorMenuItem = new javax.swing.JMenuItem();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    centerPanel.setLayout(new java.awt.BorderLayout());

    jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
    jSplitPane1.setLeftComponent(tabPane);

    totalLossPanel.setLayout(new java.awt.BorderLayout());

    trendPanel.setLayout(new java.awt.BorderLayout());
    totalLossPanel.add(trendPanel, java.awt.BorderLayout.CENTER);

    valuePanel.setLayout(new java.awt.GridBagLayout());

    jSmoothLabel1.setOpaque(false);
    jSmoothLabel1.setText("Total Loss");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
    valuePanel.add(jSmoothLabel1, gridBagConstraints);

    totalLossScalarViewer.setText("-----");
    totalLossScalarViewer.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipady = 10;
    gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
    valuePanel.add(totalLossScalarViewer, gridBagConstraints);

    jSmoothLabel2.setOpaque(false);
    jSmoothLabel2.setText("Total Injection Loss");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 2;
    gridBagConstraints.insets = new java.awt.Insets(10, 5, 0, 0);
    valuePanel.add(jSmoothLabel2, gridBagConstraints);

    totalInjLossScalarViewer.setText("-----");
    totalInjLossScalarViewer.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 3;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipady = 10;
    gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
    valuePanel.add(totalInjLossScalarViewer, gridBagConstraints);

    totalLossPanel.add(valuePanel, java.awt.BorderLayout.EAST);

    jSplitPane1.setRightComponent(totalLossPanel);

    centerPanel.add(jSplitPane1, java.awt.BorderLayout.CENTER);

    getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);

    downPanel.setLayout(new java.awt.BorderLayout());

    statusPanel.setLayout(new java.awt.GridBagLayout());
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.weightx = 1.0;
    statusPanel.add(blmStatusViewer, gridBagConstraints);

    settingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Setting files", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
    settingPanel.setLayout(new java.awt.GridBagLayout());

    gainFileViewer.setText("simpleScalarViewer1");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipady = 10;
    gridBagConstraints.weightx = 1.0;
    settingPanel.add(gainFileViewer, gridBagConstraints);

    offsetFileViewer.setText("simpleScalarViewer1");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipady = 10;
    gridBagConstraints.weightx = 1.0;
    settingPanel.add(offsetFileViewer, gridBagConstraints);

    jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
    jLabel1.setText("Gain");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipady = 10;
    gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
    settingPanel.add(jLabel1, gridBagConstraints);

    jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
    jLabel2.setText("Offset");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipady = 10;
    gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
    settingPanel.add(jLabel2, gridBagConstraints);

    gainButton.setText("...");
    gainButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        gainButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    settingPanel.add(gainButton, gridBagConstraints);

    offsetButton.setText("...");
    offsetButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        offsetButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    settingPanel.add(offsetButton, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipadx = 200;
    statusPanel.add(settingPanel, gridBagConstraints);

    downPanel.add(statusPanel, java.awt.BorderLayout.CENTER);

    btnPanel.setLayout(new java.awt.GridBagLayout());
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.weightx = 1.0;
    btnPanel.add(dummyPanel, gridBagConstraints);

    zeroButton.setText("Zero Gain");
    zeroButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        zeroButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
    btnPanel.add(zeroButton, gridBagConstraints);

    resetButton.setText("Reset Errors");
    resetButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        resetButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
    btnPanel.add(resetButton, gridBagConstraints);

    EnableAllButton.setText("Enable All");
    EnableAllButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        EnableAllButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
    btnPanel.add(EnableAllButton, gridBagConstraints);

    DisableAllButton.setText("Disable All");
    DisableAllButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        DisableAllButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
    btnPanel.add(DisableAllButton, gridBagConstraints);

    resetTriggerButton.setText("Reset Tigger Counter");
    resetTriggerButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        resetTriggerButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
    btnPanel.add(resetTriggerButton, gridBagConstraints);

    StartButton.setText("Start Injection");
    StartButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        StartButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
    btnPanel.add(StartButton, gridBagConstraints);

    StopButton.setText("Stop Injection");
    StopButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        StopButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
    btnPanel.add(StopButton, gridBagConstraints);

    btnSettingPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

    jLabel5.setText("Zero gain when Current too low");
    btnSettingPanel.add(jLabel5);
    btnSettingPanel.add(zeroGainComboEditor);

    jLabel4.setText("Auto Calibration");
    btnSettingPanel.add(jLabel4);
    btnSettingPanel.add(autoCalibComboEditor);

    jLabel3.setText("Injection HDB Storage");
    btnSettingPanel.add(jLabel3);
    btnSettingPanel.add(injectionComboEditor);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.gridwidth = 8;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    btnPanel.add(btnSettingPanel, gridBagConstraints);

    downPanel.add(btnPanel, java.awt.BorderLayout.SOUTH);

    getContentPane().add(downPanel, java.awt.BorderLayout.SOUTH);

    fileMenu.setText("File");

    loadOffsetMenuItem.setText("Load Offsets...");
    loadOffsetMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        loadOffsetMenuItemActionPerformed(evt);
      }
    });
    fileMenu.add(loadOffsetMenuItem);

    saveOffsetMenuItem.setText("Save Offsets...");
    saveOffsetMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        saveOffsetMenuItemActionPerformed(evt);
      }
    });
    fileMenu.add(saveOffsetMenuItem);

    loadGainMenuItem.setText("Load Gains...");
    loadGainMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        loadGainMenuItemActionPerformed(evt);
      }
    });
    fileMenu.add(loadGainMenuItem);

    saveGainMenuItem.setText("Save gains...");
    saveGainMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        saveGainMenuItemActionPerformed(evt);
      }
    });
    fileMenu.add(saveGainMenuItem);

    exitMenuItem.setText("Exit");
    exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        exitMenuItemActionPerformed(evt);
      }
    });
    fileMenu.add(exitMenuItem);

    jMenuBar1.add(fileMenu);

    viewMenu.setText("View");

    expertMenuItem.setText("Expert Mode");
    expertMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        expertMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(expertMenuItem);

    diagMenuItem.setText("Diagnostics...");
    diagMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        diagMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(diagMenuItem);

    errorMenuItem.setText("Errors...");
    errorMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        errorMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(errorMenuItem);

    jMenuBar1.add(viewMenu);

    setJMenuBar(jMenuBar1);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
    ATKDiagnostic.showDiagnostic();
  }//GEN-LAST:event_diagMenuItemActionPerformed

  private void errorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorMenuItemActionPerformed
    ATKGraphicsUtils.centerFrameOnScreen(errWin);
    errWin.setVisible(true);    
  }//GEN-LAST:event_errorMenuItemActionPerformed

  private void zeroButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zeroButtonActionPerformed


    try {
      DeviceProxy ds = DeviceFactory.getInstance().getDevice(BLMAllName);
      DeviceAttribute daA = new DeviceAttribute("BldVgcOutputA",0.0);
      DeviceAttribute daB = new DeviceAttribute("BldVgcOutputB",0.0);
      DeviceAttribute daC = new DeviceAttribute("BldVgcOutputC",0.0);
      DeviceAttribute daD = new DeviceAttribute("BldVgcOutputD",0.0);
      ds.write_attribute(daA);
      ds.write_attribute(daB);
      ds.write_attribute(daC);
      ds.write_attribute(daD);
    } catch(ConnectionException e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    }
    
  }//GEN-LAST:event_zeroButtonActionPerformed

  private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
    exitForm();
  }//GEN-LAST:event_exitMenuItemActionPerformed

  private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed

    try {
      DeviceProxy ds = DeviceFactory.getInstance().getDevice(BLMAllName);
      ds.command_inout("ResetError");
    } catch(ConnectionException e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    }
    
  }//GEN-LAST:event_resetButtonActionPerformed

  private void loadOffsetMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadOffsetMenuItemActionPerformed
    smOffsets.loadSettingsFile();
  }//GEN-LAST:event_loadOffsetMenuItemActionPerformed

  private void saveOffsetMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveOffsetMenuItemActionPerformed
    try {
      // Force setpoint refresh
      DeviceProxy ds = DeviceFactory.getInstance().getDevice(BLMAllName);
      //ds.command_inout("UpdateSetpoint");
      ds.read_attribute("Offsets");
      // Call setting manager
      smOffsets.saveSettingsFile();
    } catch (DevFailed ex) {
      ErrorPane.showErrorMessage(this,offsetsSettingManagerName,ex);
    } catch(ConnectionException e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    }
  }//GEN-LAST:event_saveOffsetMenuItemActionPerformed

  private void loadGainMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadGainMenuItemActionPerformed
      smGains.loadSettingsFile();
  }//GEN-LAST:event_loadGainMenuItemActionPerformed

  private void saveGainMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveGainMenuItemActionPerformed
    try {
      // Force setpoint refresh
      DeviceProxy ds = DeviceFactory.getInstance().getDevice(BLMAllName);
      //ds.command_inout("UpdateSetpoint");
      ds.read_attribute("VGC");      
      // Call setting manager
      smGains.saveSettingsFile();
    } catch (DevFailed ex) {
      ErrorPane.showErrorMessage(this,gainsSettingManagerName,ex);
    } catch(ConnectionException e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    }
  }//GEN-LAST:event_saveGainMenuItemActionPerformed

  private void resetTriggerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetTriggerButtonActionPerformed
    try {
      DeviceProxy ds = DeviceFactory.getInstance().getDevice(BLMAllName);
      ds.command_inout("ResetTriggerCounter");
    } catch(ConnectionException e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    }
  }//GEN-LAST:event_resetTriggerButtonActionPerformed

  private void gainButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gainButtonActionPerformed

    try {

      String status;
      DeviceAttribute da = smGains.getDevice().read_attribute("Status");
      status = da.extractString();
      JOptionPane.showMessageDialog(this,status,"Setting Manager Status ["+gainsSettingManagerName+"]",JOptionPane.INFORMATION_MESSAGE);

    } catch (DevFailed ex) {
      ErrorPane.showErrorMessage(this, gainsSettingManagerName, ex);
    }

  }//GEN-LAST:event_gainButtonActionPerformed

  private void offsetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_offsetButtonActionPerformed
    try {

      String status;
      DeviceAttribute da = smOffsets.getDevice().read_attribute("Status");
      status = da.extractString();
      JOptionPane.showMessageDialog(this,status,"Setting Manager Status ["+offsetsSettingManagerName+"]",JOptionPane.INFORMATION_MESSAGE);

    } catch (DevFailed ex) {
      ErrorPane.showErrorMessage(this, offsetsSettingManagerName, ex);
    }
  }//GEN-LAST:event_offsetButtonActionPerformed

  private void EnableAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EnableAllButtonActionPerformed

    try {
      DeviceProxy ds = DeviceFactory.getInstance().getDevice(BLMAllName);
      ds.command_inout("EnableAll");
    } catch(ConnectionException e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    }
    
  }//GEN-LAST:event_EnableAllButtonActionPerformed

  private void DisableAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DisableAllButtonActionPerformed
    
    try {
      DeviceProxy ds = DeviceFactory.getInstance().getDevice(BLMAllName);
      ds.command_inout("DisableAll");
    } catch(ConnectionException e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    }
    
  }//GEN-LAST:event_DisableAllButtonActionPerformed

  private void expertMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expertMenuItemActionPerformed

    if(expertMenuItem.isSelected()) {
      stateViewer.setToggleVisible(true);
      btnPanel.setVisible(true);
      settingPanel.setVisible(true);
    } else {
      stateViewer.setToggleVisible(false);
      btnPanel.setVisible(false);
      settingPanel.setVisible(false);
    }
    revalidate();
    
  }//GEN-LAST:event_expertMenuItemActionPerformed

  private void StartButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StartButtonActionPerformed

    try {
      DeviceProxy ds = DeviceFactory.getInstance().getDevice(BLMAllName);
      ds.command_inout("ForceStartInjection");
    } catch(ConnectionException e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    }

  }//GEN-LAST:event_StartButtonActionPerformed

  private void StopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StopButtonActionPerformed

    try {
      DeviceProxy ds = DeviceFactory.getInstance().getDevice(BLMAllName);
      ds.command_inout("ForceStopInjection");
    } catch(ConnectionException e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(this, BLMAllName, e);
    }

  }//GEN-LAST:event_StopButtonActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    new MainPanel().setVisible(true);
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton DisableAllButton;
  private javax.swing.JButton EnableAllButton;
  private javax.swing.JButton StartButton;
  private javax.swing.JButton StopButton;
  private fr.esrf.tangoatk.widget.attribute.BooleanScalarComboEditor autoCalibComboEditor;
  private fr.esrf.tangoatk.widget.attribute.StatusViewer blmStatusViewer;
  private javax.swing.JPanel btnPanel;
  private javax.swing.JPanel btnSettingPanel;
  private javax.swing.JPanel centerPanel;
  private javax.swing.JMenuItem diagMenuItem;
  private javax.swing.JPanel downPanel;
  private javax.swing.JPanel dummyPanel;
  private javax.swing.JMenuItem errorMenuItem;
  private javax.swing.JMenuItem exitMenuItem;
  private javax.swing.JCheckBoxMenuItem expertMenuItem;
  private javax.swing.JMenu fileMenu;
  private javax.swing.JButton gainButton;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer gainFileViewer;
  private fr.esrf.tangoatk.widget.attribute.EnumScalarComboEditor injectionComboEditor;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JMenuBar jMenuBar1;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel jSmoothLabel1;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel jSmoothLabel2;
  private javax.swing.JSplitPane jSplitPane1;
  private javax.swing.JMenuItem loadGainMenuItem;
  private javax.swing.JMenuItem loadOffsetMenuItem;
  private javax.swing.JButton offsetButton;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer offsetFileViewer;
  private javax.swing.JButton resetButton;
  private javax.swing.JButton resetTriggerButton;
  private javax.swing.JMenuItem saveGainMenuItem;
  private javax.swing.JMenuItem saveOffsetMenuItem;
  private javax.swing.JPanel settingPanel;
  private javax.swing.JPanel statusPanel;
  private javax.swing.JTabbedPane tabPane;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer totalInjLossScalarViewer;
  private javax.swing.JPanel totalLossPanel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer totalLossScalarViewer;
  private javax.swing.JPanel trendPanel;
  private javax.swing.JPanel valuePanel;
  private javax.swing.JMenu viewMenu;
  private javax.swing.JButton zeroButton;
  private fr.esrf.tangoatk.widget.attribute.BooleanScalarComboEditor zeroGainComboEditor;
  // End of variables declaration//GEN-END:variables

  @Override
  public void booleanScalarChange(BooleanScalarEvent bse) {
    boolean b = bse.getValue();

    /*
    if(b != lastInjection ) {
      if(b)
        tabPane.setSelectedIndex(1);
      else
        tabPane.setSelectedIndex(0);
    }
    */
    
    lastInjection = b;
  }

  @Override
  public void stateChange(AttributeStateEvent ase) {
  }

  @Override
  public void errorChange(ErrorEvent ee) {
  }
  
}
